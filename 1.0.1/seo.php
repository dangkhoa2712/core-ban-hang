<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class SeoMigration_101
 */
class SeoMigration_101 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('seo', [
                'columns' => [
                    new Column(
                        'seo_id',
                        [
                            'type'          => Column::TYPE_INTEGER,
                            'notNull'       => true,
                            'autoIncrement' => true,
                            'size'          => 11,
                            'first'         => true,
                        ]
                    ),
                    new Column(
                        'seo_description',
                        [
                            'type'    => Column::TYPE_TEXT,
                            'notNull' => true,
                            'size'    => 1,
                            'after'   => 'seo_id',
                        ]
                    ),
                    new Column(
                        'seo_keywords',
                        [
                            'type'    => Column::TYPE_VARCHAR,
                            'notNull' => false,
                            'size'    => 255,
                            'after'   => 'seo_description',
                        ]
                    ),
                    new Column(
                        'seo_title',
                        [
                            'type'    => Column::TYPE_TEXT,
                            'notNull' => true,
                            'size'    => 1,
                            'after'   => 'seo_keywords',
                        ]
                    ),
                    new Column(
                        'obj_id',
                        [
                            'type'    => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size'    => 11,
                            'after'   => 'seo_title',
                        ]
                    ),
                    new Column(
                        'obj_type',
                        [
                            'type'    => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size'    => 11,
                            'after'   => 'obj_id',
                        ]
                    ),
                ],
                'indexes' => [
                    new Index('PRIMARY', ['seo_id'], 'PRIMARY'),
                ],
                'options' => [
                    'TABLE_TYPE'      => 'BASE TABLE',
                    'AUTO_INCREMENT'  => '17',
                    'ENGINE'          => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci',
                ],
            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
