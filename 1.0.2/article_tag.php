<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ArticleTagMigration_102
 */
class ArticleTagMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('article_tag', array(
                'columns' => array(
                    new Column(
                        'at_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'tag_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'at_id'
                        )
                    ),
                    new Column(
                        'article_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'tag_id'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('at_id'), 'PRIMARY'),
                    new Index('index_tag_id', array('tag_id'), null),
                    new Index('index_article_id', array('article_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'at_article_id',
                        array(
                            'referencedTable' => 'article',
                            'columns' => array('article_id'),
                            'referencedColumns' => array('article_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'at_tag_id',
                        array(
                            'referencedTable' => 'tag',
                            'columns' => array('tag_id'),
                            'referencedColumns' => array('tag_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_unicode_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
