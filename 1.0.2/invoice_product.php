<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class InvoiceProductMigration_102
 */
class InvoiceProductMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('invoice_product', array(
                'columns' => array(
                    new Column(
                        'invoice_product_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'invoice_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'invoice_product_id'
                        )
                    ),
                    new Column(
                        'product_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'invoice_id'
                        )
                    ),
                    new Column(
                        'invoice_product_quantity',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_id'
                        )
                    ),
                    new Column(
                        'invoice_product_price',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'notNull' => true,
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'invoice_product_quantity'
                        )
                    ),
                    new Column(
                        'invoice_product_tax',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'default' => "0.0000",
                            'notNull' => true,
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'invoice_product_price'
                        )
                    ),
                    new Column(
                        'invoice_product_total_price',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'notNull' => true,
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'invoice_product_tax'
                        )
                    ),
                    new Column(
                        'invoice_product_model',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'invoice_product_total_price'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('invoice_product_id'), 'PRIMARY'),
                    new Index('invoice_has_product_FKIndex1', array('invoice_id'), null),
                    new Index('invoice_has_product_FKIndex2', array('product_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'invoice_product_ibfk_1',
                        array(
                            'referencedTable' => 'invoice',
                            'columns' => array('invoice_id'),
                            'referencedColumns' => array('invoice_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'invoice_product_ibfk_2',
                        array(
                            'referencedTable' => 'product',
                            'columns' => array('product_id'),
                            'referencedColumns' => array('product_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
