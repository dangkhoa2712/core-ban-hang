<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ProductDiscountMigration_102
 */
class ProductDiscountMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('product_discount', array(
                'columns' => array(
                    new Column(
                        'product_discount_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'product_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_discount_id'
                        )
                    ),
                    new Column(
                        'product_discount_priority',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_id'
                        )
                    ),
                    new Column(
                        'product_discount_price',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'default' => "1.0000",
                            'notNull' => true,
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'product_discount_priority'
                        )
                    ),
                    new Column(
                        'product_discount_quantity',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "0",
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_discount_price'
                        )
                    ),
                    new Column(
                        'product_discount_start',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_discount_quantity'
                        )
                    ),
                    new Column(
                        'product_discount_end',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_discount_start'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('product_discount_id', 'product_id'), 'PRIMARY'),
                    new Index('product_discount_FKIndex1', array('product_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'product_discount_ibfk_1',
                        array(
                            'referencedTable' => 'product',
                            'columns' => array('product_id'),
                            'referencedColumns' => array('product_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '1',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
