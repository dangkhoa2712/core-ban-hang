<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ProductHasAttributeMigration_102
 */
class ProductHasAttributeMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('product_has_attribute', array(
                'columns' => array(
                    new Column(
                        'product_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'attribute_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_id'
                        )
                    ),
                    new Column(
                        'language_id',
                        array(
                            'type' => Column::TYPE_CHAR,
                            'notNull' => true,
                            'size' => 2,
                            'after' => 'attribute_id'
                        )
                    ),
                    new Column(
                        'pha_attribute_text',
                        array(
                            'type' => Column::TYPE_TEXT,
                            'size' => 1,
                            'after' => 'language_id'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('product_id', 'attribute_id', 'language_id'), 'PRIMARY'),
                    new Index('product_has_attribute_FKIndex1', array('product_id'), null),
                    new Index('product_has_attribute_FKIndex2', array('attribute_id'), null),
                    new Index('product_has_attribute_FKIndex3', array('language_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'product_has_attribute_ibfk_1',
                        array(
                            'referencedTable' => 'product',
                            'columns' => array('product_id'),
                            'referencedColumns' => array('product_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'product_has_attribute_ibfk_2',
                        array(
                            'referencedTable' => 'attribute',
                            'columns' => array('attribute_id'),
                            'referencedColumns' => array('attribute_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'product_has_attribute_ibfk_3',
                        array(
                            'referencedTable' => 'language',
                            'columns' => array('language_id'),
                            'referencedColumns' => array('language_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
