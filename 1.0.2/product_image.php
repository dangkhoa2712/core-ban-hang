<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ProductImageMigration_102
 */
class ProductImageMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('product_image', array(
                'columns' => array(
                    new Column(
                        'product_image_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'product_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_image_id'
                        )
                    ),
                    new Column(
                        'product_image_url',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 255,
                            'after' => 'product_id'
                        )
                    ),
                    new Column(
                        'product_image_type',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "0",
                            'unsigned' => true,
                            'size' => 4,
                            'after' => 'product_image_url'
                        )
                    ),
                    new Column(
                        'product_image_weight',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "1",
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 4,
                            'after' => 'product_image_type'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('product_image_id', 'product_id'), 'PRIMARY'),
                    new Index('product_image_FKIndex1', array('product_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'product_image_ibfk_1',
                        array(
                            'referencedTable' => 'product',
                            'columns' => array('product_id'),
                            'referencedColumns' => array('product_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '1',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
