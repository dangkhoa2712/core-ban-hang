<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class AttributeDescMigration_102
 */
class AttributeDescMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('attribute_desc', array(
                'columns' => array(
                    new Column(
                        'attribute_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'language_id',
                        array(
                            'type' => Column::TYPE_CHAR,
                            'notNull' => true,
                            'size' => 2,
                            'after' => 'attribute_id'
                        )
                    ),
                    new Column(
                        'attribute_desc_text',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 255,
                            'after' => 'language_id'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('attribute_id', 'language_id'), 'PRIMARY'),
                    new Index('attribute_desc_FKIndex1', array('attribute_id'), null),
                    new Index('attribute_desc_FKIndex2', array('language_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'attribute_desc_ibfk_1',
                        array(
                            'referencedTable' => 'attribute',
                            'columns' => array('attribute_id'),
                            'referencedColumns' => array('attribute_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'attribute_desc_ibfk_2',
                        array(
                            'referencedTable' => 'language',
                            'columns' => array('language_id'),
                            'referencedColumns' => array('language_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
