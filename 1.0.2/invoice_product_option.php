<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class InvoiceProductOptionMigration_102
 */
class InvoiceProductOptionMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('invoice_product_option', array(
                'columns' => array(
                    new Column(
                        'invoice_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'product_cost_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'invoice_id'
                        )
                    ),
                    new Column(
                        'ipo_quantity',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_cost_id'
                        )
                    ),
                    new Column(
                        'ipo_price',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'ipo_quantity'
                        )
                    ),
                    new Column(
                        'ipo_tax',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'ipo_price'
                        )
                    ),
                    new Column(
                        'ipo_total_price',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'size' => 11,
                            'after' => 'ipo_tax'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('invoice_id', 'product_cost_id'), 'PRIMARY'),
                    new Index('invoice_product_option_FKIndex1', array('invoice_id'), null),
                    new Index('invoice_product_option_FKIndex2', array('product_cost_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'invoice_product_option_ibfk_1',
                        array(
                            'referencedTable' => 'invoice',
                            'columns' => array('invoice_id'),
                            'referencedColumns' => array('invoice_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'invoice_product_option_ibfk_2',
                        array(
                            'referencedTable' => 'product_cost',
                            'columns' => array('product_cost_id'),
                            'referencedColumns' => array('product_cost_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
