<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ProductDescMigration_102
 */
class ProductDescMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('product_desc', array(
                'columns' => array(
                    new Column(
                        'product_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'language_id',
                        array(
                            'type' => Column::TYPE_CHAR,
                            'notNull' => true,
                            'size' => 2,
                            'after' => 'product_id'
                        )
                    ),
                    new Column(
                        'product_name',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 100,
                            'after' => 'language_id'
                        )
                    ),
                    new Column(
                        'product_desc',
                        array(
                            'type' => Column::TYPE_TEXT,
                            'size' => 1,
                            'after' => 'product_name'
                        )
                    ),
                    new Column(
                        'product_meta_title',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'product_desc'
                        )
                    ),
                    new Column(
                        'product_meta_description',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'product_meta_title'
                        )
                    ),
                    new Column(
                        'product_meta_keyword',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'product_meta_description'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('product_id', 'language_id'), 'PRIMARY'),
                    new Index('product_desc_FKIndex1', array('product_id'), null),
                    new Index('product_desc_FKIndex2', array('language_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'product_desc_ibfk_1',
                        array(
                            'referencedTable' => 'product',
                            'columns' => array('product_id'),
                            'referencedColumns' => array('product_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'product_desc_ibfk_2',
                        array(
                            'referencedTable' => 'language',
                            'columns' => array('language_id'),
                            'referencedColumns' => array('language_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '1',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
