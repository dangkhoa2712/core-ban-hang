<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ProductCategoryDescMigration_102
 */
class ProductCategoryDescMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('product_category_desc', array(
                'columns' => array(
                    new Column(
                        'language_id',
                        array(
                            'type' => Column::TYPE_CHAR,
                            'notNull' => true,
                            'size' => 2,
                            'first' => true
                        )
                    ),
                    new Column(
                        'product_category_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'language_id'
                        )
                    ),
                    new Column(
                        'pcd_category_name',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 50,
                            'after' => 'product_category_id'
                        )
                    ),
                    new Column(
                        'pcd_category_desc',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'pcd_category_name'
                        )
                    ),
                    new Column(
                        'pcd_meta_title',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'pcd_category_desc'
                        )
                    ),
                    new Column(
                        'pcd_meta_description',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'pcd_meta_title'
                        )
                    ),
                    new Column(
                        'pcd_meta_keyword',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'pcd_meta_description'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('language_id', 'product_category_id'), 'PRIMARY'),
                    new Index('language_has_product_category_FKIndex1', array('language_id'), null),
                    new Index('language_has_product_category_FKIndex2', array('product_category_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'product_category_desc_ibfk_1',
                        array(
                            'referencedTable' => 'language',
                            'columns' => array('language_id'),
                            'referencedColumns' => array('language_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'product_category_desc_ibfk_2',
                        array(
                            'referencedTable' => 'product_category',
                            'columns' => array('product_category_id'),
                            'referencedColumns' => array('product_category_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
