<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ProductCostDescMigration_102
 */
class ProductCostDescMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('product_cost_desc', array(
                'columns' => array(
                    new Column(
                        'language_id',
                        array(
                            'type' => Column::TYPE_CHAR,
                            'notNull' => true,
                            'size' => 2,
                            'first' => true
                        )
                    ),
                    new Column(
                        'product_cost_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'language_id'
                        )
                    ),
                    new Column(
                        'pcd_attribute_text',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'product_cost_id'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('language_id', 'product_cost_id'), 'PRIMARY'),
                    new Index('product_cost_has_language_FKIndex2', array('language_id'), null),
                    new Index('product_cost_desc_FKIndex2', array('product_cost_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'product_cost_desc_ibfk_1',
                        array(
                            'referencedTable' => 'product_cost',
                            'columns' => array('product_cost_id'),
                            'referencedColumns' => array('product_cost_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'product_cost_desc_ibfk_2',
                        array(
                            'referencedTable' => 'language',
                            'columns' => array('language_id'),
                            'referencedColumns' => array('language_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
