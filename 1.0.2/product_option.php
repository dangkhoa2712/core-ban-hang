<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ProductOptionMigration_102
 */
class ProductOptionMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('product_option', array(
                'columns' => array(
                    new Column(
                        'product_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'manufacturer_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'size' => 11,
                            'after' => 'product_id'
                        )
                    ),
                    new Column(
                        'product_rate',
                        array(
                            'type' => Column::TYPE_FLOAT,
                            'size' => 15,
                            'scale' => 2,
                            'after' => 'manufacturer_id'
                        )
                    ),
                    new Column(
                        'product_minimun',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "1",
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_rate'
                        )
                    ),
                    new Column(
                        'product_maximun',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "0",
                            'size' => 11,
                            'after' => 'product_minimun'
                        )
                    ),
                    new Column(
                        'product_shipping',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'default' => "1.0000",
                            'notNull' => true,
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'product_maximun'
                        )
                    ),
                    new Column(
                        'product_view',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "0",
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_shipping'
                        )
                    ),
                    new Column(
                        'product_market_price',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'default' => "0.0000",
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'product_view'
                        )
                    ),
                    new Column(
                        'product_tax_rare',
                        array(
                            'type' => Column::TYPE_FLOAT,
                            'default' => "0.00",
                            'notNull' => true,
                            'size' => 4,
                            'scale' => 2,
                            'after' => 'product_market_price'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('product_id'), 'PRIMARY'),
                    new Index('product_option_FKIndex1', array('product_id'), null),
                    new Index('product_option_FKIndex2', array('manufacturer_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'product_option_ibfk_1',
                        array(
                            'referencedTable' => 'product',
                            'columns' => array('product_id'),
                            'referencedColumns' => array('product_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'product_option_ibfk_2',
                        array(
                            'referencedTable' => 'manufacturer',
                            'columns' => array('manufacturer_id'),
                            'referencedColumns' => array('manufacturer_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
