<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class InvoiceMigration_102
 */
class InvoiceMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('invoice', array(
                'columns' => array(
                    new Column(
                        'invoice_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'user_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "0",
                            'size' => 11,
                            'after' => 'invoice_id'
                        )
                    ),
                    new Column(
                        'invoice_url',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 100,
                            'after' => 'user_id'
                        )
                    ),
                    new Column(
                        'invoice_full_name',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 50,
                            'after' => 'invoice_url'
                        )
                    ),
                    new Column(
                        'invoice_phone',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 12,
                            'after' => 'invoice_full_name'
                        )
                    ),
                    new Column(
                        'invoice_address',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 100,
                            'after' => 'invoice_phone'
                        )
                    ),
                    new Column(
                        'invoice_email',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 50,
                            'after' => 'invoice_address'
                        )
                    ),
                    new Column(
                        'invoice_payment_full_name',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 50,
                            'after' => 'invoice_email'
                        )
                    ),
                    new Column(
                        'invoice_payment_phone',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 12,
                            'after' => 'invoice_payment_full_name'
                        )
                    ),
                    new Column(
                        'invoice_payment_address',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 100,
                            'after' => 'invoice_payment_phone'
                        )
                    ),
                    new Column(
                        'invoice_total',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'notNull' => true,
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'invoice_payment_address'
                        )
                    ),
                    new Column(
                        'invoice_tax',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'default' => "0.0000",
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'invoice_total'
                        )
                    ),
                    new Column(
                        'invoice_created',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'invoice_tax'
                        )
                    ),
                    new Column(
                        'invoice_updated',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'invoice_created'
                        )
                    ),
                    new Column(
                        'invoice_shipper',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 50,
                            'after' => 'invoice_updated'
                        )
                    ),
                    new Column(
                        'invoice_user_agent',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'invoice_shipper'
                        )
                    ),
                    new Column(
                        'invoice_ip',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 40,
                            'after' => 'invoice_user_agent'
                        )
                    ),
                    new Column(
                        'invoice_status',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "0",
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 4,
                            'after' => 'invoice_ip'
                        )
                    ),
                    new Column(
                        'invoice_creator',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "0",
                            'unsigned' => true,
                            'size' => 11,
                            'after' => 'invoice_status'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('invoice_id'), 'PRIMARY')
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
