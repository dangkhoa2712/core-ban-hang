<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ReviewMigration_102
 */
class ReviewMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('review', array(
                'columns' => array(
                    new Column(
                        'review_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 10,
                            'first' => true
                        )
                    ),
                    new Column(
                        'product_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'review_id'
                        )
                    ),
                    new Column(
                        'review_name',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 50,
                            'after' => 'product_id'
                        )
                    ),
                    new Column(
                        'review_content',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 255,
                            'after' => 'review_name'
                        )
                    ),
                    new Column(
                        'review_rate',
                        array(
                            'type' => Column::TYPE_FLOAT,
                            'notNull' => true,
                            'size' => 3,
                            'scale' => 2,
                            'after' => 'review_content'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('review_id', 'product_id'), 'PRIMARY'),
                    new Index('review_FKIndex1', array('product_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'review_ibfk_1',
                        array(
                            'referencedTable' => 'product',
                            'columns' => array('product_id'),
                            'referencedColumns' => array('product_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '1',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
