<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ProductMigration_102
 */
class ProductMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('product', array(
                'columns' => array(
                    new Column(
                        'product_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'product_price',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'notNull' => true,
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'product_id'
                        )
                    ),
                    new Column(
                        'product_unit',
                        array(
                            'type' => Column::TYPE_FLOAT,
                            'default' => "0.00",
                            'size' => 4,
                            'scale' => 2,
                            'after' => 'product_price'
                        )
                    ),
                    new Column(
                        'product_weight',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "1",
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_unit'
                        )
                    ),
                    new Column(
                        'product_status',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "0",
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 4,
                            'after' => 'product_weight'
                        )
                    ),
                    new Column(
                        'product_created',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_status'
                        )
                    ),
                    new Column(
                        'product_updated',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_created'
                        )
                    ),
                    new Column(
                        'product_quantity',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "0",
                            'size' => 11,
                            'after' => 'product_updated'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('product_id'), 'PRIMARY')
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '1',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
