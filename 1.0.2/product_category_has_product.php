<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ProductCategoryHasProductMigration_102
 */
class ProductCategoryHasProductMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('product_category_has_product', array(
                'columns' => array(
                    new Column(
                        'pchp_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'product_category_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'pchp_id'
                        )
                    ),
                    new Column(
                        'product_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_category_id'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('pchp_id'), 'PRIMARY'),
                    new Index('product_category_has_product_unit_key', array('product_category_id', 'product_id'), 'UNIQUE'),
                    new Index('product_category_has_product_FKIndex1', array('product_category_id'), null),
                    new Index('product_category_has_product_FKIndex2', array('product_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'product_category_has_product_ibfk_1',
                        array(
                            'referencedTable' => 'product_category',
                            'columns' => array('product_category_id'),
                            'referencedColumns' => array('product_category_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'product_category_has_product_ibfk_2',
                        array(
                            'referencedTable' => 'product',
                            'columns' => array('product_id'),
                            'referencedColumns' => array('product_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
