<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ProductCostMigration_102
 */
class ProductCostMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('product_cost', array(
                'columns' => array(
                    new Column(
                        'product_cost_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'product_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_cost_id'
                        )
                    ),
                    new Column(
                        'attribute_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'product_id'
                        )
                    ),
                    new Column(
                        'pc_price',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'notNull' => true,
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'attribute_id'
                        )
                    ),
                    new Column(
                        'pc_market_price',
                        array(
                            'type' => Column::TYPE_DECIMAL,
                            'size' => 15,
                            'scale' => 4,
                            'after' => 'pc_price'
                        )
                    ),
                    new Column(
                        'pc_quantity',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'default' => "0",
                            'size' => 11,
                            'after' => 'pc_market_price'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('product_cost_id'), 'PRIMARY'),
                    new Index('product_cost_FKIndex1', array('attribute_id'), null),
                    new Index('product_cost_FKIndex2', array('product_id'), null),
                    new Index('product_cost_unit_index', array('product_id', 'attribute_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'product_cost_ibfk_1',
                        array(
                            'referencedTable' => 'product',
                            'columns' => array('product_id'),
                            'referencedColumns' => array('product_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'product_cost_ibfk_2',
                        array(
                            'referencedTable' => 'attribute',
                            'columns' => array('attribute_id'),
                            'referencedColumns' => array('attribute_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
