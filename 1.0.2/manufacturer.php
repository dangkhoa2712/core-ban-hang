<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ManufacturerMigration_102
 */
class ManufacturerMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('manufacturer', array(
                'columns' => array(
                    new Column(
                        'manufacturer_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'manufacturer_name',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 50,
                            'after' => 'manufacturer_id'
                        )
                    ),
                    new Column(
                        'manufacturer_desc',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'manufacturer_name'
                        )
                    ),
                    new Column(
                        'manufacturer_image',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'manufacturer_desc'
                        )
                    ),
                    new Column(
                        'manufacturer_weight',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'size' => 4,
                            'after' => 'manufacturer_image'
                        )
                    ),
                    new Column(
                        'manufacturer_status',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'size' => 4,
                            'after' => 'manufacturer_weight'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('manufacturer_id'), 'PRIMARY')
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '1',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
