<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class CurrencyMigration_102
 */
class CurrencyMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('currency', array(
                'columns' => array(
                    new Column(
                        'currency_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'currency_tittle',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 32,
                            'after' => 'currency_id'
                        )
                    ),
                    new Column(
                        'currency_code',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 3,
                            'after' => 'currency_tittle'
                        )
                    ),
                    new Column(
                        'currency_symbol_left',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 12,
                            'after' => 'currency_code'
                        )
                    ),
                    new Column(
                        'currency_symbol_right',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 12,
                            'after' => 'currency_symbol_left'
                        )
                    ),
                    new Column(
                        'currency_decimal_place',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 4,
                            'after' => 'currency_symbol_right'
                        )
                    ),
                    new Column(
                        'currency_value',
                        array(
                            'type' => Column::TYPE_FLOAT,
                            'notNull' => true,
                            'size' => 15,
                            'scale' => 8,
                            'after' => 'currency_decimal_place'
                        )
                    ),
                    new Column(
                        'currency_status',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 4,
                            'after' => 'currency_value'
                        )
                    ),
                    new Column(
                        'currency_created',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'currency_status'
                        )
                    ),
                    new Column(
                        'currency_updated',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'currency_created'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('currency_id'), 'PRIMARY')
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '1',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
