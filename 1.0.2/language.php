<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Mvc\Model\Migration;

/**
 * Class LanguageMigration_102
 */
class LanguageMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('language', [
                'columns' => [
                    new Column(
                        'language_id',
                        [
                            'type'    => Column::TYPE_CHAR,
                            'notNull' => true,
                            'size'    => 2,
                            'first'   => true,
                        ]
                    ),
                    new Column(
                        'language_name',
                        [
                            'type'  => Column::TYPE_VARCHAR,
                            'size'  => 20,
                            'after' => 'language_id',
                        ]
                    ),
                ],
                'indexes' => [
                    new Index('PRIMARY', ['language_id'], 'PRIMARY'),
                ],
                'options' => [
                    'TABLE_TYPE'      => 'BASE TABLE',
                    'AUTO_INCREMENT'  => '',
                    'ENGINE'          => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci',
                ],
            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

        self::$_connection->delete( 'language',
            'language_id = "vi"');
        self::$_connection->insert(
            'language',
            ['vi', 'Việt Nam']);
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
